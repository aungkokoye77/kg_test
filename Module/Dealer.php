<?php

/**
 * Created by PhpStorm.
 * User: aung.ko
 */
namespace Module;

use phpDocumentor\Reflection\Types\Array_;

class Dealer
{
    private $newDeck;

    public function __construct($deck)
    {
        $this->newDeck = $deck;
    }

    /**
     * create shuffle deck
     * return array
     */
    public function getShuffleDeck() {
        $half = count($this->newDeck) / 2;
        $firstHalfDesk = array_slice($this->newDeck, 0, $half);
        $secondHalfDesk = array_slice($this->newDeck, $half, $half);
        $i = 0;
        $shuffle=array();
        foreach ($firstHalfDesk as $card) {
            $shuffle[] = $card;
            $shuffle[] = $secondHalfDesk[$i];
            $i++;
        }
        return $shuffle;
    }

    /**
     * create each player cards
     * return array
     */
    public function dealer($player, $eachPlayerCards, Array $deck) {
        $playerCounter=1;
        $cardCounter=1;
        $result = array();
        if ($player * $eachPlayerCards > count($deck)) {
            return 'Not Enough Cards!';
        }
        foreach ($deck as $card) {
            if($cardCounter > $player * $eachPlayerCards ) {
                break;
            }
            $result[$playerCounter][]   = $card ;
            $playerCounter = ($playerCounter === $player) ? 1 : $playerCounter+1;
            $cardCounter++;
        }
        return $result;
    }

    /**
     * For PHPUnit test
     * return array
     */
    public function getNewDeck() {
        return $this->newDeck;
    }
}