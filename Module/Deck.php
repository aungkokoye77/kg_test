<?php

/**
 * Created by PhpStorm.
 * User: aung.ko
 */
namespace Module;

class Deck
{
    private $suits = array('Heart', 'Club', 'Spade', 'Diamond');
    private $cards = array('Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King');
    public $deck = array();

    /**
     * create sequence deck
     * return array
     */
    public function getDeck()
    {
        if(!$this->deck) {
            foreach ($this->suits as $suit) {
                foreach ($this->cards as $card) {
                    $this->deck[] = $suit . '-' . $card;
                }
            }
        }
        return $this->deck;
    }

    /**
     * create sequence deck
     * return array
     */
    public function getSuits() {
        return $this->suits;
    }

    /**
     * create sequence deck
     * return array
     */
    public function getCards() {
        return $this->cards;
    }
}