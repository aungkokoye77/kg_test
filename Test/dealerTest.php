<?php
/**
 * Created by PhpStorm.
 * User: aungkoko
 */

namespace test;

require __DIR__ .'/../vendor/autoload.php';
use Module\Deck;
use Module\Dealer;

class dealerTest extends \PHPUnit_Framework_TestCase
{

    public function dealerProvider()
    {
        return [
            [2, 5,  10],
            [4, 7, 28],
            [4, 13, 52],
            [4, 14, 'Not Enough Cards!']
        ];
    }

    public function testConstruct()
    {
        $Deck = new Deck();
        $Dealer = new Dealer($Deck->getDeck());
        $this->assertEquals(52, count($Dealer->getNewDeck()));

        return $Dealer;
    }

    /**
     * @depends testConstruct
     */
    public function testShuffle(Dealer $Dealer)
    {
        $Deck = new Deck();
        $deckArray  = $Deck->getDeck();
        $Dealer = new Dealer($deckArray);
        $pre = null;
        $result = true;
        foreach ($Dealer->getShuffleDeck() as $card) {
            if($pre) {
                $preKey = array_search($pre,$deckArray);
                $currentKey = array_search($card,$deckArray);
                if($preKey == ($currentKey - 1)) {
                    $result = false;
                }
            }
            $pre = $card;
        }
        $this->assertTrue($result);

        return $Dealer;
    }

    /**
     * @depends testShuffle
     * @dataProvider dealerProvider
     */
    public function testDealer($player, $eachPlayerCards,$expected,Dealer $Dealer)
    {
        $result = $Dealer->dealer($player, $eachPlayerCards, $Dealer->getShuffleDeck());
        if(is_array($result)) {
            $arrayCount = 0;
            foreach ($result as $eachPlayerCards) {
                $arrayCount = $arrayCount + count($eachPlayerCards);
            }
            $this->assertEquals($expected, $arrayCount);
        } else {
            $this->assertEquals($expected, $result);
        }
    }
}