<?php
/**
 * Created by PhpStorm.
 * User: aungkoko
 */

namespace test;

require __DIR__ .'/../vendor/autoload.php';
use Module\Deck;

class deskTest extends \PHPUnit_Framework_TestCase
{
    public function testGetDeck()
    {
        $Deck = new Deck();
        $this->assertEquals(52, count($Deck->getDeck()));
    }

    public function testSuitSequence() {
        $Deck = new Deck();
        $suits = array('Heart', 'Club', 'Spade', 'Diamond');
        $this->assertEquals($suits, $Deck->getSuits());
    }

    public function testCardsSequence() {
        $Deck = new Deck();
        $cards = array('Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King');
        $this->assertEquals($cards, $Deck->getCards());
    }
}